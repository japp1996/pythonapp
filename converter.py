# coding=utf-8
# Python will default to ASCII as standard encoding if no other encoding hints are given. To define a source code encoding, a magic comment must be placed into the source files either as first or second line in the file, such as: # coding=

def foreign_exchange_calculator(amount):
    dolar = 8285.53
    return amount / dolar 
    
def run():
    print('B I E N V E N I D O ')
    print('Convierte bolivares soberanos a dolares')
    print('')
    amount = float(raw_input('Ingresa Tus Bolivares: '))
    result = foreign_exchange_calculator(amount)
    print('Tus bolivares valen ${} dolares'.format(result))
    print('')


if __name__ == '__main__':
    run()