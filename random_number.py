import random

def run():
    number_found = False
    random_number = random.randint(0, 20)
    #not False is True
    #not True is False
    while not number_found:
        number = int(input('Intenta un numero: '))
        if number == random_number:
            print('Felicidades, haz acertado')
            number_found = True
        elif number > random_number:
            print('El numero que ingresaste es mayor')
        else:
            print('El numero que ingresaste es menor')            

if __name__ == '__main__':
    run()