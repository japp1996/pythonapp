# coding=utf-8
# after install ascii_py we run pip install pillow colorama --upgrade 
import random

IMAGES = ['''
    
    +---+
    |   |
        |
        |
        |
        |
        |
        =========''', '''

    +---+
    |   |
    O   |
        |
        |
        |
        =========''', '''

    +---+
    |   |
    O   |
    |   |
        |
        |
        =========''', '''

    +---+
    |   |
    O   |
   /|   |
        |
        |
        =========''', '''

    +---+
    |   |
    O   |
   /|\  |
        |
        |
        =========''', '''

    +---+
    |   |
    O   |
   /|\  |
    |   |
        |
        =========''', '''

    +---+
    |   |
    O   |
   /|\  |
    |   |
   /    |
        =========''', '''

    +---+
    |   |
    O   |
   /|\  |
    |   |
   / \  |
        =========''', '''
    
''']

WORDS = [
    'lavadora',
    'secadora',
    'sofa',
    'gobierno',
    'diputado',
    'democracia',
    'computadora',
    'teclado'
]

def random_word():
    idx = random.randint(0, len(WORDS)-1)
    return WORDS[idx]

def display_boards(hidden_word, tries):
    print(IMAGES[tries])
    print('')
    print(hidden_word)
    print('--- * --- * --- * --- * --- *')

def run():
    word = random_word()
    hidden_word = ['_'] * len(word)
    tries = 0
    while True:
        display_boards(hidden_word, tries)
        current_letter = str(input('Escoge una letra: '))
        letter_index = []
        for idx in range(len(word)):
            if word[idx] == current_letter :
                letter_index.append(idx)

        if len(letter_index) == 0 :
            tries += 1
            if tries == 7 :
                display_boards(hidden_word, tries)
                print('')
                print('L O   S E N T I M O S   P E R D I S T E')
                print('La palabra correcta era {}'.format(word))
                break
        else :
            for idx in letter_index :
                hidden_word[idx] = current_letter
            letter_index = []

        try:
            hidden_word.index('_')
        except ValueError:
            display_boards(hidden_word, tries)
            print('')    
            print('F E L I C I D A D E S   G A N A S T E')
            break

if __name__ == '__main__':
    print('B I E N V E N I D O S   A L   J U E G O   A H O R C A D O S')
    run()