# coding=utf-8

def binary_search(numbers, number_to_search, low, high):
    if low > high:
        return False
    
    mid = int((low + high)/2)
    
    if numbers[mid] == number_to_search:
        return True
    elif numbers[mid] > number_to_search:
        return binary_search(numbers, number_to_search, low, mid - 1)
    elif numbers[mid] < number_to_search:
        return binary_search(numbers, number_to_search, mid + 1, high)
        
    

if __name__ == '__main__':
    numbers = [1, 3, 4, 5, 6, 9, 10, 11, 25, 27, 28, 34, 36, 49, 51]
    number_to_search = int(input('Ingresa un número: '))
    result = binary_search(numbers, number_to_search, 0, len(numbers) - 1)
    if result:
        print('El numero si está en la lista')
    else:
        print('El número no se encuentra en la lista')