print(range(5))
print(range(5, 40, 3))

for i in range(5):
    print(i)

for i in range(30):
    if i % 3 != 0:
        continue
    elif i == 22:
        break
    else:
        print(i**2)

country = 'Venezuela'

for letter in country:
    print(letter)