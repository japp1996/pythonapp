# coding=utf-8
# Python will default to ASCII as standard encoding if no other encoding hints are given. To define a source code encoding, a magic comment must be placed into the source files either as first or second line in the file, such as: # coding=

def is_prime(number):
    if number < 2:    
        return False
    elif number == 2:
        return True
    elif number > 2 and number % 2 == 0:
        return False
    else:
        for i in range(3, number):
            if number % i == 0:
                return False
            
    return True
        
    
def run():
    number = int(raw_input('Escribe un número: '))
    result = is_prime(number)
    if result:
        print('Tu numero es primo')
    else: 
        print("No es primo, es abuelo")
    
        
        

if __name__ == '__main__':
    run()