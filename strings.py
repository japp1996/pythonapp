# Ways to configure the codification
# 1) coding=utf-8
# 2) -*- coding: utf-8 -*-

phrase = 'hola'
new = 'l' + phrase[1:]
print('He reemplazado {} por {}'.format(phrase, new))

#compare with > and < its result will come by the first letter of the string order by abecedary
print('C O M P A R A C I O N E S   DE   S T R I N G S')

stringOne = 'Artificial'
stringTwo = 'Buenas practicas'
stringThree = 'Comunidad'
stringFour = 'Data Science'
stringFive = 'Inteligencia'

#False
print(stringOne > stringTwo)
#False
print(stringTwo > stringThree)
#True
print(stringThree < stringFour)
#False
print(stringFive < stringFour)

#Python 2 Take ASCII but Python 3 asumes UNICODE
# this u prefix is implemented to use characters unicode 

u'String'

# other way to codify string is r used by regex
r''

print('M A N E J O   DE   S T R I N G S')

my_string = 'japp'
print(my_string[0])
print(my_string[1])
print(my_string[2])
print(my_string[3])

#length letter of a string
print(len(my_string))
#last letter of a string
print(my_string[len(my_string) - 1])

#list of methods of strings

print('L I S T A   D E   M E T O D O S')
print(my_string.upper())
print(my_string.isupper())
print(my_string.lower())
print(my_string.islower())
indice = my_string.find('a')
print(my_string[indice])
print(my_string.isdigit())
languages = 'こんにちは'
print(languages.endswith('は'))
print(my_string.startswith('こん'))
print(my_string.split())
print(my_string.join('1996'))

print('S L I C E S   S E P A R A R   C A D E N A S   E N   T E X T O S')

print(my_string[1:])
print(my_string[1:3])
print(my_string[1:4])
print(my_string[1:4:2])
print(my_string[::-1])