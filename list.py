amigos = list()
amigos.append('Pedro')
amigos.append('Henrique')
amigos.append('Alberto')
print(amigos[0])

my_list = []
print(type(my_list))
my_list.append(1)
my_second_list = [2, 3, 4]
print(my_second_list)
my_third_list = my_list + my_second_list
my_fourth_list = my_second_list + my_list
print(my_third_list)
print(my_fourth_list)
my_fifth_list = ['a']
my_six_list = my_fifth_list * 5
print(my_six_list)
print(my_third_list[1:3])
print(my_third_list[1:4:2])
# ordering by desc
print(my_third_list[::-1])
list_for = [5,6,2,3,4,1]
list_for.pop()
list_for.sort()
print(list_for)
del list_for[0]
print(list_for)
list_of = [7, 8, 9]
list_for.extend(list_of)
print(list_for)
casa = 'casa'
lista_casa = list(casa)
str_casa = ''.join(lista_casa)
print(str_casa)