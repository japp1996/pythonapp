
my_dictionary = {}
my_object = dict()

my_dictionary['first_element'] = 'Hola'
my_dictionary['second_element'] = 'Hi'
my_dictionary['thrid_element'] = 'Bon'
my_dictionary['primer_elemento'] = 'Ciao'

print(my_dictionary['first_element'])

califications = {}
califications['algorithms'] = 9
califications['mathematics'] = 10
califications['programming'] = 8
califications['database'] = 10

total_califications = 0

for value in califications.values():
    total_califications += value

for value in califications:
    print(value)

for value in califications.values():
    print(value)
    
# for python 2 is calification.iteritems() and python 3 calification.items()
for key, value in califications.items():
    print('llave: {}, valor: {}'.format(key, value))
    
print('El total de las calificaciones es: {}'.format(total_califications))
print('El promedio del estudiante es: {}'.format(total_califications/len(califications)))